# @edvolution/log

> Logging for Node using Winston

## Usage

**Logging**

```javascript
const log = require("@edvolution/log");
log.info("Hello World");
```

**Express**

```javascript
const { logMiddleware } = require("@edvolution/log");

app.use(logMiddleware);
```

## Levels

> Each level is given a specific integer priority. The higher the priority the more important the message is considered to be, and the lower the corresponding integer priority.
>
> [https://github.com/winstonjs/winston#logging-levels](https://github.com/winstonjs/winston#logging-levels)

| Level   | Priority | Description                      |
|---------|----------|----------------------------------|
| error   | 0        | Error conditions                 |
| warn    | 1        | Warning conditions               |
| info    | 2        | Normal but significant condition |
| verbose | 3        | Informational message            |
| debug   | 4        | Debug-level message              |
