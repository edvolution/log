const morgan = require("morgan");
const winston = require("winston");

// Initialize and define options
const { combine } = winston.format;

const options = level => ({
  console: {
    colorize: true,
    format: combine(
      winston.format.splat(),
      winston.format.colorize({ all: true }),
      winston.format.label({ label: "[edvolution]" }),
      winston.format.timestamp({ format: "HH:MM:SS" }),
      winston.format.printf(
        info => `${info.label} ${info.timestamp} ${info.message}`
      )
    ),
    handleExceptions: true,
    level
  }
});

const opts = options(process.env.LOG_LEVEL || "info");

// Create loggers with options
const { error, warn, info, verbose, debug } = winston.createLogger({
  exitOnError: false,
  transports: [new winston.transports.Console(opts.console)]
});

const stream = {
  write: message => {
    info(message.substring(0, message.lastIndexOf("\n")));
  }
};

// Middleware for console output
const logMiddleware = morgan("tiny", { stream });

// Export logging utilities
module.exports = {
  error,
  warn,
  info,
  verbose,
  debug,
  logMiddleware
};
